/**
 * AquaControllerNano.h - Software for AquaController device.
 * Original Copyright (c) 2017 Vadim Teselkin.
 * web: www.pcb.aquagomel.ru
 * mail-to: Dr.Jarold@gmail.com
 * All right reserved.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <OneWire.h>
#include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <DallasTemperature.h>
#include <ArduinoJson.h>
#include <DS1307RTC.h>
#include <time.h>

LiquidCrystal_I2C lcd(0x00, 20, 4); // @suppress("Abstract class cannot be instantiated")

#define LOW  0
#define HIGH  1
/** --------------------------------------Variable block--------------------------------------- */

// Variable pointer to current menu and cursor position
word MenuIndex = 0;
//Number of lines in the Main Menu
#define MAIN_MENU_SIZE  4
//Number of lines in the Timer menu
#define MENU_TIMERS_SIZE 4
//Number of rows in the Other menu
#define MENU_OTHER_SIZE 4
//Number of lines in the WIFI menu
#define MENU_WIFI_SIZE 2
//Number of lines in the LCD menu
#define MENU_LCD_SIZE  4
//Maximum number of channels
#define MAX_CHANALS 8
//Maximum number of PWM channels
#define MAX_CHANALS_PWM 3
//Maximum number of timers
#define MAX_TIMERS 10
//Maximum number of PWM timers
#define MAX_TIMERS_PWM 4
//State timers
#define ENABLE_TIMER 1
#define DISABLE_TIMER 0

//State temperature sensors
#define DISCONNECT_SENSOR 0
#define CONNECT_SENSOR 2

//CHANAL HANDS CONTROLL
#define OFF_CHANAL 1
#define ON_CHANAL 2
#define AUTO_CHANAL 3

//Current state canals timer
#define TIMER_OFF 1
#define TIMER_ON 2
#define TIMER_MIN 3
#define TIMER_OTHER 4
#define TIMER_SEC 5
#define TIMER_TEMP 6
#define TIMER_PWM 7

//Accuracy of the temperature sensor
#define TEMPERATURE_PRECISION 11
//Maximum number of temperature sensors
#define MAX_TEMP_SENSOR 4
//Used Arduino terminals for temperature sensors
#define TEMP_SENS 3

#define SEC_IN_MIN 60

// Enabled channels for relays
const byte nRelayDrive[MAX_CHANALS] = { 6, 7, 8, 9, 10, 11, 12, 13 };

// Pin for joystick button
const byte switchPin = 5;
//Pin for speaker
const byte tonePin = 4;
// Water level sensor
const word WaterLevel = A0;
// Pin for the X axis of the joystick
const word pinX = A6;
// Pin for the Y axis of the joystick
const word pinY = A7;

// Maximum deviation for joystick operation
const word JoysticMaxX = 800;
// Minimum deviation for joystick operation
const word JoysticMinX = 150;
// Maximum deviation for joystick operation
const word JoysticMaxY = 800;
// Minimum deviation for joystick operation
const word JoysticMinY = 150;

// Flag for protection against multiple calls of the command processing method from the joystick
bool isButtonClick = false;

// Flag to protect against multiple calls to the method of handling the button on the four-way controller
bool prevSw = true;
// Delay multiplier for back lighting and return to the main menu
const byte DelayForMenu = 15;

/**
 * Menu list
 */
const char* MainMenuItems[MAIN_MENU_SIZE] = { "  Manual Control", "  Set Timers", "  Set Temperature",
		"  Other Settings" };
const char* TimersMenuItems[MENU_TIMERS_SIZE] = { "  Set Daily Timers", "  Set Seconds Timers", "  Set Other Timers" };
const char* OtherMenuItems[MENU_OTHER_SIZE] = { "  Set Date and Time", "  Activate DS18B20", "  Settings device",
		"  Set WiFi Config" };
const char* OtherWiFiMenuItems[MENU_WIFI_SIZE] = { "  WiFi module:", "  NTP Update:" };
const char* OtherLCDMenuItems[MENU_LCD_SIZE] = { "  Delay Light", "  Delay Return", "  Sensitivity", "  Sound" };

//Previous menu item. Required to check the change of the cursor position on the screen or change the screen
word prevMenuIndex = 0;

boolean needClearTimeSettings = true;

const byte HOUR = 23;
const byte MINUTE = 59;
const byte DAY = 31;
const byte MOUNTH = 12;
const byte SECONDS = 255;
const unsigned long SECOND_BY_DAY = 86399;
const word MIN_BY_DAY = 1440;

const byte TIME_FOR_ZERO_DELAY_30 = 30;
const byte TIME_FOR_ZERO_DELAY_5 = 5;
const byte CHANAL_BTN_DISABLE = 0;
unsigned long eventTime = 0;

byte minForDisableZeroChanal = 0;
bool isNeedEnableZeroCanal = false;
bool isTempCanalWarning = false;
byte sensorIndexWarning = 0;
// The array of the current Channel Status
byte stateChanals[MAX_CHANALS] = { 0, 0, 0, 0, 0, 0, 0, 0 };

unsigned int _timerForCheck = 0;
unsigned int _hourTimerForCheck = 0;
unsigned int _secondTimerForCheck = 0;

// Array of types of timers that enabled the channel
byte CurrentStateChanalsByTypeTimer[MAX_CHANALS] = { 1, 1, 1, 1, 1, 1, 1, 1 };

/**
 * Daily timer program settings
 */
byte DailyTimerHourStart[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte DailyTimerHourEnd[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte DailyTimerMinStart[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte DailyTimerMinEnd[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte DailyTimerState[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte DailyTimerChanal[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
/**
 * Second timer program settings
 */
byte SecondTimerHourStart[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte SecondTimerMinStart[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte SecondTimerDuration[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte SecondTimerState[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte SecondTimerCanal[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/**
 * Status of hourly timer programs
 */
byte HoursTimerMinStart[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte HoursTimerMinStop[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte HoursTimerState[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
byte HoursTimerCanal[MAX_TIMERS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

const byte FREQURENCY_SEND_TEMP = 10;
// Minimal step of temperature change
const byte STEP = 25;
// Minimum temperature index: MIN_TEMP + STEP* MIN_INDEX_TEMP
const byte MIN_INDEX_TEMP = 0;
// The maximum possible temperature index: MIN_TEMP + STEP* MAX_INDEX_TEMP
const byte MAX_INDEX_TEMP = 76;
// The lowest possible setting is the temperature
const word MIN_TEMP = 1600;
// Maximum possible maximum temperature
const word MAX_TEMP = 3500;
// The flag of protection against multiple calls of the CheckStateTempTimer method
bool isWaitTemp = false;
/**
 * Status of programs for temperature sensors
 */
byte TempTimerState[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };
byte TempSensorState[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };
byte TempTimerMinStart[MAX_TEMP_SENSOR] = { MIN_INDEX_TEMP, MIN_INDEX_TEMP, MIN_INDEX_TEMP, MIN_INDEX_TEMP };
byte TempTimerMaxEnd[MAX_TEMP_SENSOR] = { MAX_INDEX_TEMP, MAX_INDEX_TEMP, MAX_INDEX_TEMP, MAX_INDEX_TEMP };
byte TempSensor[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };
byte TempTimerChanal[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };

byte NULL_ARRAY[1] = { 255 };

/**
 * Addressing memory to store device states
 */
const byte DailyTimerHourStartAddr = 255;
const byte DailyTimerHourEndAddr = 245;
const byte DailyTimerMinStartAddr = 235;
const byte DailyTimerMinEndAddr = 225;
const byte DailyTimerStateAddr = 215;
const byte DailyTimerChanalAddr = 205;

const byte SecondTimerHourStartAddr = 195;
const byte SecondTimerMinStartAddr = 185;
const byte SecondTimerDurationAddr = 175;
const byte SecondTimerStateAddr = 165;
const byte SecondTimerCanalAddr = 155;

const byte DailyHoursTimerMinStartAddr = 145;
const byte DailyHoursTimerMinDurationAddr = 135;
const byte DailyHoursTimerStateAddr = 125;
const byte DailyHoursTimerCanalAddr = 115;

const byte TempTimerMinStartAddr = 105;
const byte TempTimerMaxEndAddr = 101;
const byte TempTimerStateAddr = 97;
const byte TempTimerChanalAddr = 93;

const word AUTO_CONNECT_ADDR = 1000;
const word NTP_UPDATE_ADDR = 998;
const word LCD_LED_ON_ADDR = 996;
const word LCD_BACK_ADDR = 994;
const word LCD_BUTTON_ADDR = 992;
const word LCD_SOUND_ADDR = 990;
const word LCD_I2C_ADDR = 988;
const word addrTempSensor = 980;

const byte addrChanals = 20;

// Flag of the first device start. Required for cleaning EEPROM
const byte ADDR_FIRST_LAUNCH = 0;

/**
 * Current indices of selected timers
 */
byte indexTimer = 0;
byte indexSecondsTimer = 0;
byte indexHoursTimer = 0;
byte indexTempTimer = 0;

// Settings for updating the date and time by NTP
bool NTP_UPDATE = true;
// Communication module settings
bool AUTO_CONNECT = true;
// Speaker Setup
bool isTone = true;
// Array of constants for adjusting the sensitivity of the joystick: DelayLCDButton[i] * 100
const byte DelayLCDButton[5] = { 2, 3, 4, 5, 6 };
bool isWarning = true;
// Indexes of current device settings
byte indexDelayLCDLedOn = 0;
byte indexDelayLCDBackInmainScreen = 0;
byte indexDelayLCDButton = 2;

unsigned long lastTimeButton;
bool isLCDLightActive = true;

OneWire oneWire(TEMP_SENS);
// DS18B20 Connect to 3 pin
DallasTemperature ds(&oneWire);
tmElements_t tm; // @suppress("Type cannot be resolved")

bool isUpdateTemp = false;
DeviceAddress addrThermometer[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };
DeviceAddress addrNewThermometer[MAX_TEMP_SENSOR] = { 0, 0, 0, 0 };
const char* wifilog;
bool isSettingsComplete = false;
bool isPostComplete = true;
/** ----------------------------------------Init metod---------------------------------- */

void setup() {

	OnFirstLunch();
	Wire.begin();
	Serial.begin(9600);

	pinMode(pinX, INPUT_PULLUP);
	pinMode(pinY, INPUT_PULLUP);
	//pinMode(WaterLevel, INPUT_PULLUP);
	pinMode(switchPin, INPUT_PULLUP);

	pinMode(tonePin, OUTPUT);

	for (byte i = 0; i < MAX_CHANALS; i++) {
		pinMode(nRelayDrive[i], OUTPUT);
		digitalWrite(nRelayDrive[i], LOW); //OFF
	}
	if (scanI2CBus()) {
		Tone(1200, 10, 2000);
		LoadTempTimerFromERROM();
		LoadWiFiSettings();
		LoadLcdSetings();
		LoadChanelState();
		LoadTimersReadFromERROM();
		LoadSecondsTimersReadFromERROM();
		LoadHoursTimersReadFromERROM();
		InitDalasSensor();
		DrawMainScreen();
	} else {
		Tone(300, 2000, 0);
	}

}
/** ----------------------------------------Loop metod---------------------------------- */
void loop() {
	RTC.read(tm);
	CheckTimeLCDEvent();
	CheckStateTimer(_timerForCheck, DailyTimerState, DailyTimerChanal,
	TIMER_MIN, CheckStateDailyTimer);
	CheckStateTimer(_hourTimerForCheck, HoursTimerState, HoursTimerCanal,
	TIMER_OTHER, CheckStateHoursTimer);
	CheckStateTimer(_secondTimerForCheck, SecondTimerState, SecondTimerCanal,
	TIMER_SEC, CheckStateSecondTimer);
	GetTemperature();
	TemporaryDisconnectionChanal(true, 0);
	CheckStateTempTimer();
	SetStateCanalByTimers();
	DrawMenu(false);
	CheckWaterLevel();
	ButtonClick();
	PrintLog();

}
bool isSendWaterLevel = false;
void CheckWaterLevel() {
	if (analogRead(WaterLevel) > 950) {  // read the input pin
		if (isTone)
			tone(tonePin, 300, 2000);
	}
	if (tm.Second % 10 == 0 && isSettingsComplete) {
		if (!isSendWaterLevel) {
			isSendWaterLevel = true;
			GetAlarmWaterLevel(analogRead(WaterLevel));
		}
	} else {
		isSendWaterLevel = false;
	}
}

/**
 * The method of searching for the LCD address on the I2C bus
 */
bool scanI2CBus() {
	byte adress = EEPROM.read(LCD_I2C_ADDR);
	if (adress < 10 || adress > 127) {
		byte nDevices = 0;
		for (byte address = 10; address <= 127; address++) {
			Wire.beginTransmission(address);
			byte error = Wire.endTransmission();
			if (error == 0) {
				adress = address;
				nDevices++;
			}
		}
		if (nDevices == 0) {
			return false;
		} else if (nDevices > 1) {
			return false;
		}
		EEPROM.write(LCD_I2C_ADDR, adress);
	}
	lcd = LiquidCrystal_I2C(adress, 20, 4);
	lcd.init();
	lcd.backlight();
	lcd.print(F("Aquarium Controller!  ver. 1.3.9-0.6"));

	return true;

}

// Root method for draw menu
void DrawMenu(bool isForceUpdate) {

	if (prevMenuIndex == MenuIndex && !isForceUpdate) {
		if (MenuIndex == 0) {
			DrawTimeInMainScreen(0);
			DrawTemperatureInScreen();
		} else if (MenuIndex >= 410 && MenuIndex <= 415) {
			DrawTimeInMainScreen(1);
		}
		return;
	}
	prevMenuIndex = MenuIndex;
	if (MenuIndex < 10) {
		DrawMainScreen();
	} else if (MenuIndex % 10 == 0 && MenuIndex < 100) {
		DrawSubMenu(MainMenuItems, MAIN_MENU_SIZE);
	} else if (MenuIndex >= 21 && MenuIndex <= 23) {
		DrawSubMenu(TimersMenuItems, MENU_TIMERS_SIZE);
	} else if (MenuIndex >= 31 && MenuIndex <= 35) {
		DrawSetTempTimerMenu();
	} else if (MenuIndex >= 41 && MenuIndex <= 44) {
		DrawSubMenu(OtherMenuItems, MENU_OTHER_SIZE);
	} else if (MenuIndex >= 11 && MenuIndex <= 18) {
		DrawManualControlMenu();
	} else if (MenuIndex >= 210 && MenuIndex <= 216) {
		DrawTimerMenu(indexTimer, DailyTimerState[indexTimer], DailyTimerHourStart[indexTimer],
				DailyTimerMinStart[indexTimer], DailyTimerHourEnd[indexTimer], DailyTimerMinEnd[indexTimer],
				DailyTimerChanal[indexTimer]);
	} else if (MenuIndex >= 220 && MenuIndex <= 226) {
		DrawTimerMenu(indexSecondsTimer, SecondTimerState[indexSecondsTimer], SecondTimerHourStart[indexSecondsTimer],
				SecondTimerMinStart[indexSecondsTimer], SecondTimerDuration[indexSecondsTimer], NULL_ARRAY[0],
				SecondTimerCanal[indexSecondsTimer]);
	} else if (MenuIndex >= 230 && MenuIndex <= 235) {
		DrawTimerMenu(indexHoursTimer, HoursTimerState[indexHoursTimer], HoursTimerMinStart[indexHoursTimer],
				NULL_ARRAY[0], HoursTimerMinStop[indexHoursTimer], NULL_ARRAY[0], HoursTimerCanal[indexHoursTimer]);
	} else if (MenuIndex >= 410 && MenuIndex <= 415) {
		DrawMenuSettinsTimer();
	} else if (MenuIndex == 420) {
		SetDalasSensor();
	} else if (MenuIndex >= 430 && MenuIndex <= 433) {
		DrawSubMenu(OtherLCDMenuItems, MENU_LCD_SIZE);
		ViewLcdSetings();
	} else if (MenuIndex >= 440 && MenuIndex <= 441) {
		DrawSubMenu(OtherWiFiMenuItems, MENU_WIFI_SIZE);
		ViewWiFiSettings();
	}

}

/**
 * The method of processing commands from the joystick
 */
void ButtonClick() {

	if (isButtonClick)
		return;
	isButtonClick = true;
	word DX = 512;
	word DY = 512;
	DX = analogRead(pinX);
	DY = analogRead(pinY);

	if (DY > JoysticMaxY + DelayLCDButton[indexDelayLCDButton] * 10) {
//		Serial.println(DY);
		ButtonUpOrDown(true);
	} else if (DY < JoysticMinY - DelayLCDButton[indexDelayLCDButton] * 10) {
//		Serial.println(DY);
		ButtonUpOrDown(false);
	} else if (DX > JoysticMaxX + DelayLCDButton[indexDelayLCDButton] * 10) {
//		Serial.println(DX);
		ButtonRightOrLeft(true);
	} else if (DX < JoysticMinX - DelayLCDButton[indexDelayLCDButton] * 10) {
//		Serial.println(DX);
		ButtonRightOrLeft(false);
	} else {
		ButtonSw();
	}
	if (DY > JoysticMaxY || DY < JoysticMinY || DX > JoysticMaxX || DX < JoysticMinX) {
		setLCDLightButtonClick();
		Tone(1000, 5, 150);
	} else {
		eventTime = 0;
	}
	isButtonClick = false;
}
/**
 * Method of signaling through the system speaker
 */
void Tone(const word frequency, const word duration, const word pause) {
	if (isTone) {
		if (frequency == 0 && duration == 0) {
			tone(tonePin, 500, 10);
		} else {
			tone(tonePin, frequency, duration);
		}
	}
	delay(pause);
}

void TemporaryDisconnectionChanal(bool isCheck, byte delay) {
	if (isCheck) {
		if (isNeedEnableZeroCanal) {
			if (tm.Minute == minForDisableZeroChanal) {
				digitalWrite(nRelayDrive[CHANAL_BTN_DISABLE], HIGH);
				isNeedEnableZeroCanal = false;
				CurrentStateChanalsByTypeTimer[CHANAL_BTN_DISABLE] = TIMER_ON;
				GetChanalState();
				DrawMainScreenNoClear();

			} else {
				if (digitalRead(nRelayDrive[CHANAL_BTN_DISABLE]) == HIGH) {
					digitalWrite(nRelayDrive[CHANAL_BTN_DISABLE], LOW);
					CurrentStateChanalsByTypeTimer[CHANAL_BTN_DISABLE] =
					TIMER_OFF;
					GetChanalState();
				}
				PrintLog();
			}
		}
	} else {
		if (digitalRead(nRelayDrive[CHANAL_BTN_DISABLE]) == HIGH && !isNeedEnableZeroCanal) {
			digitalWrite(nRelayDrive[CHANAL_BTN_DISABLE], LOW);
			CurrentStateChanalsByTypeTimer[CHANAL_BTN_DISABLE] = TIMER_OFF;
			minForDisableZeroChanal = tm.Minute + delay;
			isNeedEnableZeroCanal = true;
			if (minForDisableZeroChanal > MINUTE) {
				minForDisableZeroChanal -= (MINUTE + 1);
			}
			GetChanalState();
			DrawMainScreenNoClear();

		} else if (isNeedEnableZeroCanal) {
			digitalWrite(nRelayDrive[CHANAL_BTN_DISABLE], HIGH);
			CurrentStateChanalsByTypeTimer[CHANAL_BTN_DISABLE] = TIMER_ON;
			isNeedEnableZeroCanal = false;
			GetChanalState();
			DrawMainScreen();
		}
	}

}
/**
 * Method for processing the rotation of the joystick along the Y coordinate
 */
void ButtonUpOrDown(boolean isUp) {

	if (MenuIndex < 10) {
		if (eventTime == 0)
			eventTime = millis();
		if (!isUp) {
			if (eventTime && (millis() - eventTime > 3000)) {
				TemporaryDisconnectionChanal(false, TIME_FOR_ZERO_DELAY_30);
				eventTime = 0;
			}
		} else {

			if (eventTime && (millis() - eventTime > 3000)) {
				TemporaryDisconnectionChanal(false, TIME_FOR_ZERO_DELAY_5);
				eventTime = 0;
			}
		}
	} else if (MenuIndex == 10 || MenuIndex == 20 || MenuIndex == 30) {
		if (isUp)
			MenuIndex = MenuIndex - 10;
		else
			MenuIndex = MenuIndex + 10;
	} else if (MenuIndex == 40) {
		if (isUp)
			MenuIndex = MenuIndex - 10;
	} else if (MenuIndex == 11 || MenuIndex == 21 || MenuIndex == 41 || MenuIndex == 430 || MenuIndex == 440) {
		if (!isUp)
			MenuIndex++;
	} else if (MenuIndex == 18 || MenuIndex == 23 || MenuIndex == 44 || MenuIndex == 433 || MenuIndex == 441) {
		if (isUp)
			MenuIndex--;
	} else if ((MenuIndex >= 12 && MenuIndex <= 17) || MenuIndex == 42 || MenuIndex == 43 || MenuIndex == 22
			|| MenuIndex == 431 || MenuIndex == 432) {
		if (isUp)
			MenuIndex--;
		else
			MenuIndex++;
	} else if (MenuIndex == 31 || MenuIndex == 210 || MenuIndex == 220 || MenuIndex == 230) {
		ChangeIndexTimer(isUp);
	} else if (MenuIndex == 35) {
		ChangeStateTimer(TempTimerState, indexTempTimer);
	} else if (MenuIndex == 211) {
		ChangeStateTimer(DailyTimerState, indexTimer);
	} else if (MenuIndex == 221) {
		ChangeStateTimer(SecondTimerState, indexSecondsTimer);
	} else if (MenuIndex == 231) {
		ChangeStateTimer(HoursTimerState, indexHoursTimer);
	} else if (MenuIndex == 216) {
		ChangeByteValueByRegion(DailyTimerChanal[indexTimer], MAX_CHANALS - 1, 0, isUp);
		DrawMenu(true);
	} else if (MenuIndex == 32 || MenuIndex == 33) {
		ChangeTempsTimer(isUp);
	} else if (MenuIndex == 34) {
		ChangeTempChanal(isUp);
	} else if ((MenuIndex >= 212 && MenuIndex <= 215) || (MenuIndex >= 222 && MenuIndex <= 224)
			|| (MenuIndex >= 232 && MenuIndex <= 233)) {
		ChangeTimeTimer(isUp);
	} else if (MenuIndex == 225) {
		ChangeByteValueByRegion(SecondTimerCanal[indexSecondsTimer],
		MAX_CHANALS - 1, 0, isUp);
		DrawMenu(true);
	} else if (MenuIndex == 234) {
		ChangeByteValueByRegion(HoursTimerCanal[indexHoursTimer],
		MAX_CHANALS - 1, 0, isUp);
		DrawMenu(true);
	} else if (MenuIndex >= 410 && MenuIndex <= 415) {
		ChangeValueForDateTime(isUp);
	}

}
/**
 * Method for processing the rotation of the joystick in the X coordinate
 */
void ButtonRightOrLeft(bool isRight) {
	if (MenuIndex < 10) {
		if (isRight) {
			MenuIndex = 10;
		} else {
			if (eventTime == 0)
				eventTime = millis();
			if (eventTime && (millis() - eventTime > 3000)) {
				isWarning = !isWarning;
				DrawMenu(true);
				eventTime = 0;
			}
		}
	} else if (MenuIndex == 10 || MenuIndex == 20 || MenuIndex == 30 || MenuIndex == 40) {
		if (isRight)
			MenuIndex++;
		else
			MenuIndex = 0;
	} else if (MenuIndex >= 11 && MenuIndex <= 18) {
		ChangeStateChanels(isRight);
		isNeedEnableZeroCanal = false;
	} else if (MenuIndex >= 21 && MenuIndex <= 23) {
		if (isRight)
			MenuIndex = MenuIndex * 10;
		else
			MenuIndex = 20;
	} else if (MenuIndex >= 31 && MenuIndex <= 35) {
		if (isRight) {
			if (TempSensorState[indexTempTimer] != DISCONNECT_SENSOR)
				MenuIndex++;
			if (MenuIndex == 36)
				MenuIndex = 31;
		} else {
			if (TempSensorState[indexTempTimer] != DISCONNECT_SENSOR)
				MenuIndex--;
			if (MenuIndex == 30)
				MenuIndex = 35;
		}
	} else if (MenuIndex >= 41 && MenuIndex <= 44) {
		if (isRight)
			MenuIndex = MenuIndex * 10;
		else
			MenuIndex = 40;
	} else if (MenuIndex >= 210 && MenuIndex <= 216) {
		if (isRight) {
			MenuIndex++;
			if (MenuIndex == 217)
				MenuIndex = 210;
		} else {
			MenuIndex--;
			if (MenuIndex == 209)
				MenuIndex = 216;
		}
	} else if (MenuIndex >= 220 && MenuIndex <= 225) {
		if (isRight) {
			MenuIndex++;
			if (MenuIndex == 226)
				MenuIndex = 220;
		} else {
			MenuIndex--;
			if (MenuIndex == 219)
				MenuIndex = 225;
		}
	} else if (MenuIndex >= 230 && MenuIndex <= 234) {
		if (isRight) {
			MenuIndex++;
			if (MenuIndex == 235)
				MenuIndex = 230;
		} else {
			MenuIndex--;
			if (MenuIndex == 229)
				MenuIndex = 234;
		}
	} else if (MenuIndex >= 410 && MenuIndex <= 415) {
		if (isRight) {
			MenuIndex++;
			if (MenuIndex == 416)
				MenuIndex = 410;
		} else {
			MenuIndex--;
			if (MenuIndex == 409)
				MenuIndex = 415;
		}
	} else if (MenuIndex >= 430 && MenuIndex <= 433) {
		if (isRight) {
			LcdChangeValue(false);
			ViewLcdSetings();
		} else {
			MenuIndex = MenuIndex / 10;
			SaveLcdSetings();
		}
	} else if (MenuIndex == 440)
		if (isRight)
			CangeOtherWifiSettings(false);
		else {
			MenuIndex = MenuIndex / 10;
			GetWiFiSettings();
			SaveWifiSettings();
		}
	else if (MenuIndex == 441) {
		if (isRight)
			CangeOtherWifiSettings(true);
		else {
			MenuIndex = MenuIndex / 10;
			GetWiFiSettings();
			SaveWifiSettings();
		}
	} else {
		if (!isRight) {
			if (MenuIndex > 100 && MenuIndex != 210 && MenuIndex != 310)
				MenuIndex = MenuIndex / 10;
		}
	}

}

/**
 * Method for processing the button pressing on the joystick
 */
boolean ButtonSw() {
	if (digitalRead(switchPin) == 1 && prevSw == false) {
		prevSw = true;
		if (MenuIndex <= 18 && MenuIndex != 10) {
			MenuIndex = 10;
		} else if (MenuIndex >= 31 && MenuIndex <= 35) {
			SaveTempTimerToERROM();
			MenuIndex = 30;
		} else if (MenuIndex >= 210 && MenuIndex <= 217) {
			SaveTimerToERROM();
		} else if (MenuIndex >= 220 && MenuIndex <= 225) {
			SaveSecondsTimerToERROM();
		} else if (MenuIndex >= 230 && MenuIndex <= 235) {
			SaveHoursTimerToERROM();
		} else if (MenuIndex >= 410 && MenuIndex <= 415) {
			needClearTimeSettings = true;
		} else if (MenuIndex >= 430 && MenuIndex <= 433) {
			SaveLcdSetings();
		} else if (MenuIndex == 440 || MenuIndex == 441) {
			GetWiFiSettings();
			SaveWifiSettings();
		}
		if (MenuIndex >= 210)
			MenuIndex = MenuIndex / 10;
		setLCDLightButtonClick();
		Tone(0, 0, 150);
		return true;
	} else {
		prevSw = digitalRead(switchPin);
		return false;

	}
}
/**
 * Drawing the main screen with cleaning
 */
void DrawMainScreen() {
	lcd.clear();
	DrawMainScreenNoClear();
}
/**
 * Drawing the main screen without cleaning
 */
void DrawMainScreenNoClear() {
	lcd.setCursor(0, 2);
	lcd.print(F("<"));
	for (byte canal = 0; canal < MAX_CHANALS; canal++) {
		lcd.setCursor(canal + 1, 2);
		//canal ON
		if (digitalRead(nRelayDrive[canal]) == HIGH)
			lcd.print(canal + 1);
		else
			lcd.print(F("_"));
	}
	lcd.print(F(">   <_____>"));
	if (AUTO_CONNECT) {
		lcd.setCursor(14, 2);
		lcd.print(F("W"));
	}
	if (NTP_UPDATE) {
		lcd.setCursor(15, 2);
		lcd.print(F("N"));
	}

	if (indexDelayLCDLedOn != 0) {
		lcd.setCursor(16, 2);
		lcd.print(F("L"));
	}
	if (isTone) {
		lcd.setCursor(17, 2);
		lcd.print(F("S"));
	}
	if (isWarning) {
		lcd.setCursor(18, 2);
		lcd.print(F("T"));
	}
	lcd.setCursor(0, 3);
	PrintLog();

}
/**
 * Drawing the date and time for the main screen and for the date and time setting screen
 */
#define  tmYearToCal(Y) ((Y) + 1970)
void DrawTimeInMainScreen(byte col) {

	lcd.setCursor(0, col);
	print2digits(tm.Hour);
	lcd.print(F(":"));
	print2digits(tm.Minute);
	lcd.print(F(":"));
	print2digits(tm.Second);
	lcd.print(F("  "));
	print2digits(tm.Day);
	lcd.print(F("."));
	print2digits(tm.Month);
	lcd.print(F("."));
	lcd.print(tmYearToCal(tm.Year));

}
/**
 * Drawing numbers in a two-digit format "00"
 */
void print2digits(const byte number) {
	if (number < 10) {
		lcd.print(F("0"));
	}
	lcd.print(number);
}
/**
 * Drawing numbers in a three-digit format "000"
 */
void print3digits(const word number) {
	if (number < 10) {
		lcd.print(F("00"));
	} else if (number >= 10 && number < 100) {
		lcd.print(F("0"));
	}
	lcd.print(number);
}
/**
 * Drawing pseudo-real numbers to within two decimal places
 */
void printFloatDigit(const byte number) {
	word num = ConvertTempByteToWord(number);
	byte k = num / 100;
	byte m = num % 100;
	if (k < 10)
		lcd.print(F("0"));
	lcd.print(k);
	lcd.print(F("."));
	lcd.print(m);
	if (m < 10)
		lcd.print(F("0"));
}
/**
 * Drawing the device menu
 */
void DrawSubMenu(const char* menu[], const word size) {
	lcd.clear();
	for (byte i = 0; i < size; i++) {
		lcd.setCursor(0, i);
		lcd.print(menu[i]);
	}
	if (MenuIndex % 10 == 0 && MenuIndex < 100)
		lcd.setCursor(0, (MenuIndex / 10) - 1);
	else {
		if (MenuIndex < 100) {
			lcd.setCursor(0, (MenuIndex % 10) - 1);
		} else {

			lcd.setCursor(0, (MenuIndex % 10));
		}

	}
	lcd.print(F(">"));

}

/** -------------------------------Canal Logic------------------------------  */

/**
 *  Change canal state
 *  OFF_CHANAL 1             //Manual Control: OFF_CHANAL = 1
 *  ON_CHANAL 2              //Manual Control: ON_CHANAL = 2
 *  AUTO_CHANAL 3      		 //Manual Control: AUTO_CHANAL = 3
 */
void ChangeStateChanels(bool isIncrement) {
	changeStateCanal(isIncrement, MenuIndex - 11);
	SaveChanalState();
	DrawManualControlMenu();
}

/**
 *  Draw menu Manual Control
 */
void DrawManualControlMenu() {
	lcd.clear();
	byte k = 0;
	byte m = 0;
	for (byte i = 0; i < MAX_CHANALS; i++) {
		m = i;
		if (i > 3) {
			k = 10;
			m -= 4;
		}
		lcd.setCursor(0 + k, m);
		lcd.print(F("Ch"));
		lcd.print(i + 1);
		lcd.setCursor(4 + k, m);
		if (MenuIndex - 11 == i)
			lcd.print(F(">"));
		lcd.setCursor(5 + k, m);
		if (stateChanals[i] == OFF_CHANAL)
			lcd.print(F("OFF"));
		if (stateChanals[i] == ON_CHANAL)
			lcd.print(F("ON"));
		if (stateChanals[i] == AUTO_CHANAL)
			lcd.print(F("AUTO"));
	}
}
/**
 * Change state device canal
 */
void changeStateCanal(bool isIncrement, byte chanal) {
	if (isIncrement)
		stateChanals[chanal]++;
	else
		stateChanals[chanal]--;
	checkStateChanal(chanal);
}

/**
 *  Check canal device on correct state
 */
void checkStateChanal(byte chanal) {

	if (stateChanals[chanal] > AUTO_CHANAL) {
		stateChanals[chanal] = OFF_CHANAL;
		CurrentStateChanalsByTypeTimer[chanal] = TIMER_OFF;
	}
	if (stateChanals[chanal] < OFF_CHANAL) {
		stateChanals[chanal] = AUTO_CHANAL;
	}
	if (stateChanals[chanal] == OFF_CHANAL) {
		digitalWrite(nRelayDrive[chanal], LOW);
		CurrentStateChanalsByTypeTimer[chanal] = TIMER_OFF;
	} else if (stateChanals[chanal] == ON_CHANAL) {
		digitalWrite(nRelayDrive[chanal], HIGH);
		CurrentStateChanalsByTypeTimer[chanal] = TIMER_ON;
	}

}

void CheckStateAllChanal() {
	for (byte i = 0; i < MAX_CHANALS; i++) {
		checkStateChanal(i);
	}
}

/**------------------------------------ Timers logic--------------------------------------- */

/**
 *  Draw menu Timer
 */
void DrawTimerMenu(byte index, byte &state, byte &timer1, byte &timer2, byte &timer3, byte &timer4, byte &chanal) {
	lcd.clear();
	lcd.print(F("Timer   "));
	print2digits(index + 1);
	if (state == ENABLE_TIMER)
		lcd.print(F("   Enable"));
	else
		lcd.print(F("   Disable"));
	lcd.setCursor(0, 1);
	lcd.print(F("On    "));
	print2digits(timer1);
	if (timer2 < 255) {
		lcd.print(F(" : "));
		print2digits(timer2);
	}
	lcd.setCursor(0, 2);
	if (MenuIndex >= 210 && MenuIndex <= 216) {
		lcd.print(F("Off   "));
		print2digits(timer3);
	}
	if (MenuIndex >= 220 && MenuIndex <= 226) {
		lcd.print(F("Run   "));
		print3digits(timer3);
		lcd.print(F("  Seconds"));
	}
	if (MenuIndex >= 230 && MenuIndex <= 236) {
		lcd.print(F("Off   "));
		print2digits(timer3);
		lcd.print(F("  Minutes"));
	}
	if (timer4 < 255) {
		lcd.print(F(" : "));
		print2digits(timer4);
	}

	lcd.setCursor(0, 3);
	lcd.print(F("Line   "));
	print2digits(chanal + 1);

	if (MenuIndex == 210 || MenuIndex == 220 || MenuIndex == 230) {
		lcd.setCursor(7, 0);
	} else if (MenuIndex == 211 || MenuIndex == 221 || MenuIndex == 231) {
		lcd.setCursor(12, 0);
	} else if (MenuIndex == 212 || MenuIndex == 222 || MenuIndex == 232) {
		lcd.setCursor(5, 1);
	} else if (MenuIndex == 213 || MenuIndex == 223) {
		lcd.setCursor(10, 1);
	} else if (MenuIndex == 214 || MenuIndex == 224 || MenuIndex == 233) {
		lcd.setCursor(5, 2);
	} else if (MenuIndex == 215) {
		lcd.setCursor(10, 2);
	} else if (MenuIndex == 216 || MenuIndex == 225 || MenuIndex == 234) {
		lcd.setCursor(6, 3);
	}
	lcd.print(F(">"));

}

/**
 *  Every second check the status of all timers
 */
void CheckStateTimer(int lastCheck, byte TimerState[], byte TimerCanal[], byte timerType, bool (*func)(byte timer)) {

	if (tm.Second != lastCheck) {
		lastCheck = second();
		for (byte j = 0; j < MAX_CHANALS; j++) {
			bool result = false;
			for (byte i = 0; i < MAX_TIMERS; i++) {
				if (TimerState[i] == ENABLE_TIMER) {
					if (TimerCanal[i] == j && stateChanals[TimerCanal[i]] == AUTO_CHANAL) {
						if (func(i)) {
							result = true;
						}
					}
				}
			}
			CheckCollisionsOtherTimer(j, result, timerType);
		}
	}

}
/**
 * Check channel status before making any changes.
 * If we have a conflict timer switches the channel then we change the type
 * of timer is on this channel in the property stateChanalsTimer
 */
bool CheckCollisionsOtherTimer(byte chanal, bool isEnable, byte timerType) {
	if (chanal == CHANAL_BTN_DISABLE && isNeedEnableZeroCanal)
		return false;
	if (isEnable) {
		if (CurrentStateChanalsByTypeTimer[chanal] == TIMER_OFF) {
			CurrentStateChanalsByTypeTimer[chanal] = timerType;
			return true;
		} else {
			if (CurrentStateChanalsByTypeTimer[chanal] != TIMER_TEMP && timerType == TIMER_TEMP) {
				CurrentStateChanalsByTypeTimer[chanal] = timerType;
				GetChanalState();
			} else if (CurrentStateChanalsByTypeTimer[chanal] != TIMER_MIN && timerType == TIMER_MIN
					&& timerType != TIMER_TEMP) {
				CurrentStateChanalsByTypeTimer[chanal] = timerType;
				GetChanalState();
			} else if (CurrentStateChanalsByTypeTimer[chanal] != TIMER_SEC && timerType == TIMER_SEC
					&& CurrentStateChanalsByTypeTimer[chanal] != TIMER_MIN && timerType != TIMER_TEMP) {
				CurrentStateChanalsByTypeTimer[chanal] = timerType;
				GetChanalState();
			} else if (timerType == TIMER_OTHER && CurrentStateChanalsByTypeTimer[chanal] == TIMER_OFF
					&& timerType != TIMER_TEMP) {
				CurrentStateChanalsByTypeTimer[chanal] = timerType;
				GetChanalState();
			}

			return false;
		}
	} else if (CurrentStateChanalsByTypeTimer[chanal] != TIMER_OFF) {
		if (CurrentStateChanalsByTypeTimer[chanal] == timerType) {
			CurrentStateChanalsByTypeTimer[chanal] = TIMER_OFF;
			return true;
		}
	}
	return false;
}

void SetStateCanalByTimers() {

	for (byte i = 0; i < MAX_CHANALS; i++) {
		if (stateChanals[i] == AUTO_CHANAL) {
			if (CurrentStateChanalsByTypeTimer[i] != TIMER_OFF && digitalRead(nRelayDrive[i]) == LOW) {
				digitalWrite(nRelayDrive[i], HIGH);
				if (MenuIndex < 10) {
					DrawMainScreen();
				}
				GetChanalState();

			} else if (CurrentStateChanalsByTypeTimer[i] == TIMER_OFF && digitalRead(nRelayDrive[i]) == HIGH) {
				digitalWrite(nRelayDrive[i], LOW);
				if (MenuIndex < 10) {
					DrawMainScreen();
				}
				GetChanalState();
			}
		}
	}
}

/**
 *  Check on-time minute timer
 */
bool CheckStateDailyTimer(byte i) {
	if (MenuIndex >= 210 && MenuIndex <= 216) {
		return false;
	}
	word timeStart = DailyTimerHourStart[i] * SEC_IN_MIN + DailyTimerMinStart[i];
	word timeFinish = DailyTimerHourEnd[i] * SEC_IN_MIN + DailyTimerMinEnd[i];
	if (timeFinish == timeStart) {
		return false;
	}
	unsigned int currentTime = tm.Hour * SEC_IN_MIN + tm.Minute;
	if (timeFinish > timeStart) {
		if (currentTime >= timeStart && currentTime < timeFinish) {
			return true;
		}
		return false;
	} else {
		if (currentTime >= timeStart && currentTime < MIN_BY_DAY) {
			return true;
		}
		if (currentTime < timeFinish) {
			return true;
		}
	}
	return false;
}
/**
 * Checking the second timer to enable it
 */
bool CheckStateSecondTimer(byte i) {
	if (MenuIndex >= 220 && MenuIndex <= 226)
		return false;
	unsigned long timeSecondStart = (SecondTimerHourStart[i] * SEC_IN_MIN * SEC_IN_MIN) + (SecondTimerMinStart[i] * 60);
	unsigned long timeSecondFinish = timeSecondStart + SecondTimerDuration[i];
	unsigned long currentTime = tm.Hour * SEC_IN_MIN * SEC_IN_MIN + tm.Minute * SEC_IN_MIN + tm.Second;
	if (timeSecondFinish <= SECOND_BY_DAY) {
		if (currentTime >= timeSecondStart && currentTime < timeSecondFinish) {
			return true;
		}
		return false;
	} else {
		if (currentTime >= timeSecondStart && currentTime <= SECOND_BY_DAY) {
			return true;
		}
		if (currentTime < timeSecondFinish - SECOND_BY_DAY) {
			return true;
		}
		return false;
	}

}

/**
 * Checking the hourly timer to enable it
 */
bool CheckStateHoursTimer(byte i) {
	if (MenuIndex >= 230 && MenuIndex <= 235)
		return false;
	if (HoursTimerMinStart[i] == HoursTimerMinStop[i])
		return false;
	if (HoursTimerMinStart[i] < HoursTimerMinStop[i]) {
		if (tm.Minute >= HoursTimerMinStart[i] && tm.Minute < HoursTimerMinStop[i]) {
			return true;
		}
	} else {
		if (tm.Minute >= HoursTimerMinStart[i] && tm.Minute <= MINUTE) {
			return true;
		}
		if (tm.Minute < HoursTimerMinStop[i]) {
			return true;
		}
	}
	return false;
}

/**
 * Changing the index of current timers during programming
 */
void ChangeIndexTimer(bool increment) {
	if (MenuIndex == 31) {
		ChangeByteValueByRegion(indexTempTimer, ds.getDeviceCount() - 1, 0, increment);
	} else if (MenuIndex == 210) {
		ChangeByteValueByRegion(indexTimer, MAX_TIMERS - 1, 0, increment);
	} else if (MenuIndex == 220) {
		ChangeByteValueByRegion(indexSecondsTimer, MAX_TIMERS - 1, 0, increment);
	} else if (MenuIndex == 230) {
		ChangeByteValueByRegion(indexHoursTimer, MAX_TIMERS - 1, 0, increment);
	}
	DrawMenu(true);
}

/**
 * Changing the timer state
 */
void ChangeStateTimer(byte timerState[], byte index) {
	if (timerState[index] == DISABLE_TIMER)
		timerState[index] = ENABLE_TIMER;
	else
		timerState[index] = DISABLE_TIMER;
	DrawMenu(true);

}

/**
 * Changing the time when programming timers
 */
void ChangeTimeTimer(bool increment) {
	if (MenuIndex == 233) {
		ChangeByteValueByRegion(HoursTimerMinStop[indexHoursTimer], MINUTE, 0, increment);
	} else if (MenuIndex == 232) {
		ChangeByteValueByRegion(HoursTimerMinStart[indexHoursTimer], MINUTE, 0, increment);
	} else if (MenuIndex == 223) {
		ChangeByteValueByRegion(SecondTimerMinStart[indexSecondsTimer], MINUTE, 0, increment);
	} else if (MenuIndex == 213) {
		ChangeByteValueByRegion(DailyTimerMinStart[indexTimer], MINUTE, 0, increment);
	} else if (MenuIndex == 215) {
		ChangeByteValueByRegion(DailyTimerMinEnd[indexTimer], MINUTE, 0, increment);
	} else if (MenuIndex == 222) {
		ChangeByteValueByRegion(SecondTimerHourStart[indexSecondsTimer], HOUR, 0, increment);
	} else if (MenuIndex == 212) {
		ChangeByteValueByRegion(DailyTimerHourStart[indexTimer], HOUR, 0, increment);
	} else if (MenuIndex == 214) {
		ChangeByteValueByRegion(DailyTimerHourEnd[indexTimer], HOUR, 0, increment);
	} else if (MenuIndex == 224) {
		ChangeByteValueByRegion(SecondTimerDuration[indexSecondsTimer], SECONDS, 1, increment);
	}
	DrawMenu(true);
}

/**
 * Drawing the date and time settings menu
 */
void DrawMenuSettinsTimer() {
	if (MenuIndex == 410 && needClearTimeSettings) {
		lcd.clear();
		lcd.print(F("Set Date and Time:"));
		needClearTimeSettings = false;
	}
	lcd.setCursor(0, 2);
	lcd.print(F("                    "));
	if (MenuIndex == 410) {
		lcd.setCursor(0, 2);
	} else if (MenuIndex == 411) {
		lcd.setCursor(3, 2);
	} else if (MenuIndex == 412) {
		lcd.setCursor(6, 2);
	} else if (MenuIndex == 413) {
		lcd.setCursor(10, 2);
	} else if (MenuIndex == 414) {
		lcd.setCursor(13, 2);
	} else if (MenuIndex == 415) {
		lcd.setCursor(18, 2);
	}
	lcd.print(F("^^"));
}

/**
 * Changing the date and time
 */
void ChangeValueForDateTime(bool isIncrement) {

	if (MenuIndex == 410) {
		ChangeByteValueByRegion(tm.Hour, HOUR, 0, isIncrement);
	} else if (MenuIndex == 411) {
		ChangeByteValueByRegion(tm.Minute, MINUTE, 0, isIncrement);
	} else if (MenuIndex == 412) {
		tm.Second = 0;
	} else if (MenuIndex == 413) {
		ChangeByteValueByRegion(tm.Day, DAY, 1, isIncrement);
	} else if (MenuIndex == 414) {
		ChangeByteValueByRegion(tm.Month, MOUNTH, 1, isIncrement);
	} else if (MenuIndex == 415) {
		if (isIncrement)
			tm.Year++;
		else
			tm.Year--;
	}
	RTC.write(tm);
}

void ChangeByteValueByRegion(byte &data, byte max, byte min, bool increment) {
	if (increment) {
		if (data >= max)
			data = min;
		else
			data++;
	} else {
		if (data <= min)
			data = max;
		else
			data--;
	}

}

/**
 * Method for comparing addresses of DS18B20 devices
 */
bool CompareDeviceAddress(DeviceAddress &device1, DeviceAddress &device2) {
	for (byte j = 0; j < 8; j++) {
		if (device1[j] != device2[j])
			return false;
	}
	return true;
}
/**
 * Sensor initialization method DS18B20
 */
void SetDalasSensor() {
	InitDalasSensor();
	DeviceAddress device;
	byte newIndex = 0;
//Search all temperatures device
	for (byte var = 0; var < ds.getDeviceCount(); var++) {
//If we found the address of the sensor
		if (ds.getAddress(device, var)) {
			bool isNew = true;
//Looking for the address of the found device in the current address list
			for (byte i = 0; i < sizeof(addrThermometer); i++) {
				if (CompareDeviceAddress(addrThermometer[i], device)) {
					isNew = false;
				}
			}
//If the address is new, we add it to the list of new addresses
			if (isNew) {
				for (byte i = 0; i < 8; i++) {
					addrNewThermometer[newIndex][i] = device[i];
				}
				newIndex++;
			}
		}
	}

//Replace the disabled sensors with new ones
//If the new sensors a few they replace the old in accordance with the order of their addresses
	byte newIndex2 = 0;
	for (byte i = 0; i < MAX_TEMP_SENSOR; i++) {

		if (TempSensorState[i] == DISCONNECT_SENSOR && newIndex > newIndex2) {
			for (byte j = 0; j < 8; j++) {
				addrThermometer[i][j] = addrNewThermometer[newIndex2][j];
				EEPROM.write(addrTempSensor - i * 8 + j, addrThermometer[i][j]);
			}
			newIndex2++;
		}
		ds.setResolution(addrThermometer[i], TEMPERATURE_PRECISION);
	}

	printAddress();
}

/**
 * Bus initialization for temperature sensors
 */
void InitDalasSensor() {
	ds.begin();
	ds.setWaitForConversion(false);
}

/**
 * Display addresses of found sensors
 */
void printAddress() {
	lcd.clear();
	lcd.setCursor(3, 1);
	lcd.print(F("Init DS18B20!"));
	delay(2000);
	lcd.clear();
	if (ds.getDeviceCount() <= 0) {
		lcd.setCursor(0, 1);
		lcd.print(F("  Sensor not found"));
	}
	for (byte i = 0; i < ds.getDeviceCount(); i++) {
		lcd.setCursor(0, i);
		lcd.print(i + 1);
		lcd.print(F("x "));
		for (byte j = 0; j < 8; j++) {
			if (addrThermometer[i][j] < 16)
				lcd.print("0");
			lcd.print(addrThermometer[i][j], HEX);
		}

	}
	delay(3500);
	MenuIndex = MenuIndex / 10;
	lcd.clear();
}
/**
 * Obtaining temperature with DS18B20
 */
void GetTemperature() {

	if (tm.Second % FREQURENCY_SEND_TEMP != 0) {
		isUpdateTemp = false;
		return;
	}

	if (isUpdateTemp)
		return;
	isUpdateTemp = true;
	ds.requestTemperatures();
	for (byte var = 0; var < MAX_TEMP_SENSOR; var++) {
		//We obtain the temperature index
		word temp = (word) (ds.getTempC(addrThermometer[var]) * 100);

		if (temp > MAX_TEMP || temp < MIN_TEMP) {
			TempSensorState[var] = DISCONNECT_SENSOR;
			TempSensor[var] = 0;
		} else {
			TempSensorState[var] = CONNECT_SENSOR;
			TempSensor[var] = ConvertTempWordToByte(temp);
		}
	}
	DrawTemperatureInScreen();
	return;
}
/**
 * Drawing the temperature for the sensor programming screen
 */
void DrawTemperatureInScreen() {

	if (MenuIndex >= 31 && MenuIndex <= 35) {
		if (TempSensorState[indexTempTimer] != DISCONNECT_SENSOR) {
			lcd.setCursor(13, 3);
			printFloatDigit(TempSensor[indexTempTimer]);
		}
	} else if (MenuIndex == 0) {
		lcd.setCursor(0, 1);
		lcd.print(F("t1="));
		lcd.setCursor(10, 1);
		lcd.print(F("t2="));
		for (byte i = 0; i < 2; i++) {
			if (TempSensorState[i] != DISCONNECT_SENSOR) {
				lcd.setCursor(3 + 10 * i, 1);
				printFloatDigit(TempSensor[i]);
			} else {
				lcd.setCursor(3 + 10 * i, 1);
				lcd.print(F("--.--"));
			}
		}
	}

}
/**
 * Drawing the temperature sensor programming menu
 */
void DrawSetTempTimerMenu() {
	lcd.clear();
	lcd.print(F("Temp.sensor     "));
	lcd.print(indexTempTimer + 1);
	lcd.print(F("of"));
	lcd.print(ds.getDeviceCount());

	if (TempSensorState[indexTempTimer] == DISCONNECT_SENSOR) {
		if (ds.getDeviceCount() <= 0)
			lcd.clear();
		lcd.setCursor(0, 0);
		lcd.print(F("Sensor not connected"));
		lcd.setCursor(8, 1);
		lcd.print(F("or"));
		lcd.setCursor(0, 2);
		lcd.print(F("He is not activated!"));
		return;
	}

	lcd.setCursor(0, 1);
	lcd.print(F("On  "));
	printFloatDigit(TempTimerMinStart[indexTempTimer]);
	lcd.setCursor(10, 1);
	lcd.print(F("Off  "));
	printFloatDigit(TempTimerMaxEnd[indexTempTimer]);
	lcd.setCursor(0, 2);
	lcd.print(F("Line   "));
	print2digits(TempTimerChanal[indexTempTimer] + 1);
	if (TempTimerState[indexTempTimer] == ENABLE_TIMER)
		lcd.print(F("    Enable"));
	else
		lcd.print(F("    Disable"));
	lcd.setCursor(0, 3);
	if (TempTimerMinStart[indexTempTimer] < TempTimerMaxEnd[indexTempTimer])
		lcd.print(F("Warming   "));
	else if (TempTimerMinStart[indexTempTimer] > TempTimerMaxEnd[indexTempTimer])
		lcd.print(F("Cooling   "));
	else
		lcd.print(F("Error     "));
	lcd.print(F("<t=--.--C>"));
	DrawTemperatureInScreen();
	if (MenuIndex == 31) {
		lcd.setCursor(15, 0);
	} else if (MenuIndex == 32) {
		lcd.setCursor(3, 1);
	} else if (MenuIndex == 33) {
		lcd.setCursor(14, 1);
	} else if (MenuIndex == 34) {
		lcd.setCursor(6, 2);
	} else if (MenuIndex == 35) {
		lcd.setCursor(12, 2);
	}
	lcd.print(F(">"));
}

/**
 * Temperature change for programming temperature sensors
 */
void ChangeTempsTimer(bool increment) {
	if (MenuIndex == 32) {
		ChangeByteValueByRegion(TempTimerMinStart[indexTempTimer], MAX_INDEX_TEMP, MIN_INDEX_TEMP, increment);
	} else if (MenuIndex == 33) {
		ChangeByteValueByRegion(TempTimerMaxEnd[indexTempTimer], MAX_INDEX_TEMP, MIN_INDEX_TEMP, increment);
	}
	DrawMenu(true);
}

/**
 * Changing channels for programming temperature sensors
 */
void ChangeTempChanal(bool increment) {

	if (increment) {
		TempTimerChanal[indexTempTimer]++;
		if (TempTimerChanal[indexTempTimer] >= MAX_CHANALS)
			TempTimerChanal[indexTempTimer] = 0;
	} else {
		if (TempTimerChanal[indexTempTimer] <= 0)
			TempTimerChanal[indexTempTimer] = MAX_CHANALS;
		TempTimerChanal[indexTempTimer]--;
	}
	DrawMenu(true);
}

/**
 * Checking the status of programs for temperature sensors
 */
void CheckStateTempTimer() {
	if (MenuIndex >= 31 && MenuIndex <= 35)
		return;
	if (tm.Second % FREQURENCY_SEND_TEMP == 0) {
		if (!isWaitTemp) {
			isTempCanalWarning = false;
			isWaitTemp = true;
			for (byte canalIndex = 0; canalIndex < MAX_CHANALS; canalIndex++) {
				bool result = false;
				for (byte indexSensor = 0; indexSensor < MAX_TEMP_SENSOR; indexSensor++) {
					if (TempTimerChanal[indexSensor]
							== canalIndex&& TempTimerState[indexSensor] == ENABLE_TIMER && TempSensorState[indexSensor] != DISCONNECT_SENSOR) {
						if (stateChanals[TempTimerChanal[indexSensor]] == AUTO_CHANAL) {
							if (CheckStateTemp(indexSensor, canalIndex))
								result = true;
						}
					}
				}
				CheckCollisionsOtherTimer(canalIndex, result, TIMER_TEMP);
			}
		}
	} else {
		isWaitTemp = false;
	}
}

/**
 * Checking whether the program can be run for temperature sensors
 */
bool CheckStateTemp(byte sensorIndex, byte canalIndex) {

//Cooling
//-----Tmin=25------Tmax=20------/
	if (TempTimerMinStart[sensorIndex] > TempTimerMaxEnd[sensorIndex]) {
//-----Tmin=25------Tmax=20----Tcur=26--/
		if (TempSensor[sensorIndex] >= TempTimerMinStart[sensorIndex]) {
			CheckStateWarningTemp(sensorIndex, false);
			return true;
		}
//-----Tmin=25------Tmax=22----Tcur=24--/
		if (TempSensor[sensorIndex] < TempTimerMinStart[sensorIndex]
				&& TempSensor[sensorIndex] > TempTimerMaxEnd[sensorIndex]) {
			if (CurrentStateChanalsByTypeTimer[sensorIndex] == TIMER_TEMP) {
				CheckStateWarningTemp(sensorIndex, false);
				return true;
			}
		}
		CheckStateWarningTemp(sensorIndex, false);
		return false;
//Warming
//-----Tmin=20------Tmax=25------/
	} else if (TempTimerMinStart[sensorIndex] < TempTimerMaxEnd[sensorIndex]) {
//--Tcur=19---Tmin=20------Tmax=25------/
		if (TempSensor[sensorIndex] < TempTimerMinStart[sensorIndex]) {
			CheckStateWarningTemp(sensorIndex, true);
			return true;
		}
//-----Tmin=20---Tcur=22---Tmax=25------/
		if (TempSensor[sensorIndex] >= TempTimerMinStart[sensorIndex]
				&& TempSensor[sensorIndex] < TempTimerMaxEnd[sensorIndex]) {
			if (CurrentStateChanalsByTypeTimer[canalIndex] == TIMER_TEMP) {
				CheckStateWarningTemp(sensorIndex, true);
				return true;
			}
		}
		CheckStateWarningTemp(sensorIndex, true);
		return false;

	}
	return false;

}

void CheckStateWarningTemp(byte sensorIndex, bool isWarming) {

	if (isWarming) {
		if (TempSensor[sensorIndex] < TempTimerMinStart[sensorIndex]) {
			if (TempTimerMinStart[sensorIndex] - TempSensor[sensorIndex] > 4) {
				isTempCanalWarning = true;
			}
		}
		if (TempSensor[sensorIndex] > TempTimerMaxEnd[sensorIndex]) {
			if (TempSensor[sensorIndex] - TempTimerMaxEnd[sensorIndex] > 4) {
				isTempCanalWarning = true;
			}
		}
	} else {
		if (TempSensor[sensorIndex] > TempTimerMinStart[sensorIndex]) {
			if (TempSensor[sensorIndex] - TempTimerMinStart[sensorIndex] > 4) {
				isTempCanalWarning = true;
			}
		}

	}
	if (isTempCanalWarning) {
		if (MenuIndex < 10) {
			sensorIndexWarning = sensorIndex;
		}
	}

}
/**
 * Drawing the advanced device settings menu
 */
void ViewLcdSetings() {
	lcd.setCursor(15, 0);

	if (indexDelayLCDLedOn == 0) {
		lcd.print(F("<OFF>"));
	} else {
		lcd.print(F("<"));
		lcd.print(DelayForMenu * indexDelayLCDLedOn);
		lcd.print(F("s>"));

	}
	lcd.setCursor(15, 1);

	lcd.print(F("<"));
	lcd.print(DelayForMenu + (DelayForMenu * indexDelayLCDBackInmainScreen));
	lcd.print(F("s>"));

	lcd.setCursor(15, 2);
	lcd.print(F("<"));
	lcd.print(DelayLCDButton[indexDelayLCDButton] * 100);
	lcd.print(F(">"));
	lcd.setCursor(15, 3);
	if (isTone)
		lcd.print(F("<ON >"));
	else
		lcd.print(F("<OFF>"));

}
/**
 * Change advanced device settings
 */
bool LcdChangeValue(bool isSound) {
	if (isSound) {
		isTone = !isTone;
		return false;
	}
	if (MenuIndex == 430) {
		indexDelayLCDLedOn++;
		if (indexDelayLCDLedOn > 4) {
			indexDelayLCDLedOn = 0;
			return true;
		}
	} else if (MenuIndex == 431) {
		indexDelayLCDBackInmainScreen++;
		if (indexDelayLCDBackInmainScreen > 3) {
			indexDelayLCDBackInmainScreen = 0;
			return true;
		}
	} else if (MenuIndex == 432) {
		indexDelayLCDButton++;
		if (indexDelayLCDButton > 4) {
			indexDelayLCDButton = 0;
			return true;
		}
	} else if (MenuIndex == 433) {
		isTone = !isTone;
	}
	return false;
}

/**
 * Verification of the possibility of activation of system settings of the device
 */
void CheckTimeLCDEvent() {
	if (indexDelayLCDLedOn > 0) {
		if (makeTime(tm) >= lastTimeButton + DelayForMenu * indexDelayLCDLedOn) {
			if (isLCDLightActive) {
				lcd.noBacklight();
			}
			isLCDLightActive = false;

		}
	}
	if (makeTime(tm) >= lastTimeButton + DelayForMenu + (DelayForMenu * indexDelayLCDBackInmainScreen)) {
		MenuIndex = 0;
	}

}
/**
 * Turn on screen backlight when you press the buttons
 */
void setLCDLightButtonClick() {
	if (!isLCDLightActive) {
		lcd.backlight();
	}
	isLCDLightActive = true;
	lastTimeButton = makeTime(tm);
}

void ViewWiFiSettings() {
	ViewWiFiSettingsAddons(AUTO_CONNECT, 0);
	ViewWiFiSettingsAddons(NTP_UPDATE, 1);
}

void ViewWiFiSettingsAddons(bool isEnable, byte row) {
	lcd.setCursor(15, row);
	if (isEnable)
		lcd.print(F("<ON >"));
	else
		lcd.print(F("<OFF>"));
}

void CangeOtherWifiSettings(bool isNTP) {
	if (isNTP)
		NTP_UPDATE = !NTP_UPDATE;
	else
		AUTO_CONNECT = !AUTO_CONNECT;

	ViewWiFiSettings();

}
/**---------------------------------JSON----------------------------------------/
 * Flag initial initialization of the WIFI module,
 * after initialization device can send data
 */

bool isSerialEvent = false;
bool isEndMessage = false;
//byte countParam = 0;

/*
 SerialEvent occurs whenever a new data comes in the hardware serial RX. This
 routine is run between each time loop() runs, so using delay inside loop can
 delay response. Multiple bytes of data may be available.
 */
void serialEvent() {
	if (!Serial.available() || isSerialEvent)
		return;
	String inString = "";
	isEndMessage = false;
	isSerialEvent = true;
	if (Serial.available()) {
		delay(10);
	}
	while (Serial.available()) {
		char ch = Serial.read();
		switch (ch) {
		case '\n':
		case '\r':
			isEndMessage = true;
			break;
		}
		if (!isEndMessage)
			inString += ch;
		delay(1);
	}
	DynamicJsonBuffer jsonBuffer(200);
	JsonObject& root = jsonBuffer.parseObject(inString);
	if (!root.success()) {
		isSerialEvent = false;
		return;
	}

	if (inString.indexOf(F("get")) != -1) {
		if (inString.indexOf(F("c_s")) != -1) {
			GetChanalState();
		} else if (inString.indexOf(F("te_s")) != -1) {
			GetTempState();
		} else if (inString.indexOf(F("t_sen")) != -1) {
			GetRealTemp();
		} else if (inString.indexOf(F("td_s")) != -1) {
			GetDailyTimerState();
		} else if (inString.indexOf(F("th_s")) != -1) {
			GetHoursTimerState();
		} else if (inString.indexOf(F("ts_s")) != -1) {
			GetSecondsTimerState();
		} else if (inString.indexOf(F("set")) != -1) {
			GetWiFiSettings();
			isSettingsComplete = true;
		}
		isSerialEvent = false;
		return;
	} else if (inString.indexOf(F("post")) != -1) {
		isPostComplete = false;
		JsonObject& data = root[F("data")];
		setLCDLightButtonClick();
		if (inString.indexOf(F("c_s")) != -1) {
			SetJsonValue(stateChanals, MAX_CHANALS, F("c_t"), data);
			CheckStateAllChanal();
			SaveChanalState();
			isPostComplete = true;
		} else if (inString.indexOf(F("te_s")) != -1) {
			SetJsonValue(TempTimerState, MAX_TEMP_SENSOR, F("tt_s"), data);
			SetJsonValue(TempTimerMinStart, MAX_TEMP_SENSOR, F("tt_m_s"), data);
			SetJsonValue(TempTimerMaxEnd, MAX_TEMP_SENSOR, F("tt_m_e"), data);
			SetJsonValue(TempTimerChanal, MAX_TEMP_SENSOR, F("tt_c"), data);
			if (inString.indexOf(F("tt_c")) != -1) {
				SaveTempTimerToERROM();
				isPostComplete = true;
			}
		} else if (inString.indexOf(F("td_s")) != -1) {
			SetJsonValue(DailyTimerHourStart, MAX_TIMERS, F("dt_h_s"), data);
			SetJsonValue(DailyTimerHourEnd, MAX_TIMERS, F("dt_h_end"), data);
			SetJsonValue(DailyTimerMinStart, MAX_TIMERS, F("dt_m_s"), data);
			SetJsonValue(DailyTimerMinEnd, MAX_TIMERS, F("dt_m_e"), data);
			SetJsonValue(DailyTimerState, MAX_TIMERS, F("dt_s"), data);
			SetJsonValue(DailyTimerChanal, MAX_TIMERS, F("dt_c"), data);
			if (inString.indexOf(F("dt_c")) != -1) {
				SaveTimerToERROM();
				isPostComplete = true;
			}
		} else if (inString.indexOf(F("th_s")) != -1) {
			SetJsonValue(HoursTimerMinStart, MAX_TIMERS, F("ht_m_st"), data);
			SetJsonValue(HoursTimerMinStop, MAX_TIMERS, F("ht_m_sp"), data);
			SetJsonValue(HoursTimerState, MAX_TIMERS, F("ht_s"), data);
			SetJsonValue(HoursTimerCanal, MAX_TIMERS, F("ht_c"), data);
			if (inString.indexOf(F("ht_c")) != -1) {
				SaveHoursTimerToERROM();
				isPostComplete = true;
			}
		} else if (inString.indexOf("ts_s") != -1) {
			SetJsonValue(SecondTimerHourStart, MAX_TIMERS, F("st_h_s"), data);
			SetJsonValue(SecondTimerMinStart, MAX_TIMERS, F("st_m_s"), data);
			SetJsonValue(SecondTimerDuration, MAX_TIMERS, F("st_d"), data);
			SetJsonValue(SecondTimerState, MAX_TIMERS, F("st_s"), data);
			SetJsonValue(SecondTimerCanal, MAX_TIMERS, F("st_c"), data);
			if (inString.indexOf(F("st_c")) != -1) {
				SaveSecondsTimerToERROM();
				isPostComplete = true;
			}
		} else if (inString.indexOf(F("time_NTP")) != -1) {
			breakTime(data[F("epoch")], tm);
			RTC.write(tm);
		}
		DrawMenu(true);
		isSerialEvent = false;
		return;
	} else if (inString.indexOf("info") != -1) {
		if (strlen(root["log"]) == 20) {
			wifilog = root["log"];
		}

	}
	isSerialEvent = false;
}

void PrintLog() {
	if (MenuIndex < 10) {
		if (isTempCanalWarning && isWarning) {
			if (tm.Second % FREQURENCY_SEND_TEMP == 0) {
				tone(tonePin, 1200, 200);
			}
			lcd.setCursor(0, 3);
			lcd.print(F("Sensor "));
			lcd.print(sensorIndexWarning + 1);
			lcd.print(F(" Warning!!! "));
			return;
		}
		if (!isNeedEnableZeroCanal) {
			if (strlen(wifilog) == 20) {
				lcd.setCursor(0, 3);
				lcd.print(wifilog);
			}
		} else {
			lcd.setCursor(0, 3);
			lcd.print(F("Filter off "));
			if (tm.Minute > minForDisableZeroChanal) {
				lcd.print(MINUTE + 1 - tm.Minute + minForDisableZeroChanal);
			} else {
				lcd.print(minForDisableZeroChanal - tm.Minute);
			}
			lcd.print(F(" min..."));
		}
	}
}

void SendStartMess() {
	Serial.print(F("{\"status\":\"success\",\"message\":\""));
}

void SendEndMess() {
	Serial.println(F("}}"));
}

void GetAlarmWaterLevel(int level) {
	SendStartMess();
	Serial.print(F("info\",\"data\":{\"type\":"));
	Serial.print(level);
	SendEndMess();
}

/**
 {"status":"success","message":"canal","data":{"canal" : [0,0,0,0,0,0,0,0],"canal_timer" : [1,0,0,0,0,0,0,0]}}
 */
void GetChanalState() {
	delay(100);
	SendStartMess();
	Serial.print(F("c_s\",\"data\":{\"cl\""));
	GetJsonValue(CurrentStateChanalsByTypeTimer, MAX_CHANALS);
	Serial.print(F(",\"c_t\""));
	GetJsonValue(stateChanals, MAX_CHANALS);
	SendEndMess();
}

/**
 {"status":"success","message":"temp_state","data":{"temp_timer_state" : [0,0,0,0,0,0,0,0],"temp_timer_min_start" : [2200,2250,0,0,0,0,0,0],"temp_timer_max_end" : [2700,2850,0,0,0,0,0,0],"temp_timer_canal" : [5,0,0,0,0,0,0,0]}}
 */
void GetTempState() {
	delay(100);
	SendStartMess();
	Serial.print(F("te_s\",\"data\":{\"tt_s\""));
	GetJsonValue(TempTimerState, MAX_TEMP_SENSOR);
	Serial.print(F(",\"tt_m_s\""));
	GetJsonValue(TempTimerMinStart, MAX_TEMP_SENSOR);
	Serial.print(F(",\"tt_m_e\""));
	GetJsonValue(TempTimerMaxEnd, MAX_TEMP_SENSOR);
	Serial.print(F(",\"tt_c\""));
	GetJsonValue(TempTimerChanal, MAX_TEMP_SENSOR);
	SendEndMess();
}

/**
 {"status":"success","message":"temp_sensors","data":{"temp_sensors" : [2230,2890,0,0,0,0,0,0]}}
 */
void GetRealTemp() {
	SendStartMess();
	Serial.print(F("t_sen\",\"data\":{\"t_se\""));
	GetJsonValue(TempSensor, MAX_TEMP_SENSOR);
	SendEndMess();

}
/**
 {"status":"success","message":"timer_daily_state","data":{"daily_timer_hour_start" : [1,0,0,0,0,0,0,0],"daily_timer_hour_end" : [2,0,0,0,0,0,0,0],"daily_timer_min_start" : [5,0,0,0,0,0,0,0],"daily_timer_min_end" : [55,0,0,0,0,0,0,0],"daily_timer_state" : [1,0,0,0,0,0,0,0],"daily_timer_canal" : [3,0,0,0,0,0,0,0]}}
 */
void GetDailyTimerState() {
	delay(100);
	SendStartMess();
	Serial.print(F("td_s\",\"data\":{\"dt_h_s\""));
	GetJsonValue(DailyTimerHourStart, MAX_TIMERS);
	Serial.print(F(",\"dt_h_end\""));
	GetJsonValue(DailyTimerHourEnd, MAX_TIMERS);
	Serial.print(F(",\"dt_m_s\""));
	GetJsonValue(DailyTimerMinStart, MAX_TIMERS);
	Serial.print(F(",\"dt_m_e\""));
	GetJsonValue(DailyTimerMinEnd, MAX_TIMERS);
	Serial.print(F(",\"dt_s\""));
	GetJsonValue(DailyTimerState, MAX_TIMERS);
	Serial.print(F(",\"dt_c\""));
	GetJsonValue(DailyTimerChanal, MAX_TIMERS);
	SendEndMess();
}

/**
 {"status":"success","message":"timer_hours_state","data":{"hours_timer_min_start" : [45,0,0,0,0,0,0,0],"hours_timer_min_stop" : [56,0,0,0,0,0,0,0],"hours_timer_state" : [1,0,0,0,0,0,0,0],"hours_timer_canal" : [3,0,0,0,0,0,0,0]}}
 */
void GetHoursTimerState() {
	delay(150);
	SendStartMess();
	Serial.print(F("th_s\",\"data\":{\"ht_m_st\""));
	GetJsonValue(HoursTimerMinStart, MAX_TIMERS);
	Serial.print(F(",\"ht_m_sp\""));
	GetJsonValue(HoursTimerMinStop, MAX_TIMERS);
	Serial.print(F(",\"ht_s\""));
	GetJsonValue(HoursTimerState, MAX_TIMERS);
	Serial.print(F(",\"ht_c\""));
	GetJsonValue(HoursTimerCanal, MAX_TIMERS);
	SendEndMess();
}

/**
 {"status":"success","message":"timer_seconds_state","data":{"second_timer_hour_start" : [5,0,0,0,0,0,0,0],"second_timer_min_start" : [26,0,0,0,0,0,0,0],"second_timer_duration" : [960,0,0,0,0,0,0,0],"second_timer_state" : [1,0,0,0,0,0,0,0],"second_timer_canal" : [2,0,0,0,0,0,0,0]}}
 */
void GetSecondsTimerState() {
	delay(100);
	SendStartMess();
	Serial.print(F("ts_s\",\"data\":{\"st_h_s\""));
	GetJsonValue(SecondTimerHourStart, MAX_TIMERS);
	Serial.print(F(",\"st_m_s\""));
	GetJsonValue(SecondTimerMinStart, MAX_TIMERS);
	Serial.print(F(",\"st_d\""));
	GetJsonValue(SecondTimerDuration, MAX_TIMERS);
	Serial.print(F(",\"st_s\""));
	GetJsonValue(SecondTimerState, MAX_TIMERS);
	Serial.print(F(",\"st_c\""));
	GetJsonValue(SecondTimerCanal, MAX_TIMERS);
	SendEndMess();
}

/**
 {"status":"success","message":"settings","NTP": 1,"AUTO": 1, "MAX_TIMERS": 8, "MAX_TEMP_SENSOR": 4}
 */
void GetWiFiSettings() {
	SendStartMess();
	Serial.print(F("set\",\"NTP\":"));
	Serial.print(NTP_UPDATE);
	Serial.print(F(",\"AUTO\":"));
	Serial.print(AUTO_CONNECT);
	Serial.print(F(",\"TIMERS\":"));
	Serial.print(MAX_TIMERS);
	Serial.print(F(",\"SENSOR\":"));
	Serial.print(MAX_TEMP_SENSOR);
	Serial.println(F("}"));
}

void GetJsonValue(const byte arrayData[], const byte count) {
	Serial.print(F(":["));
	for (byte i = 0; i < count; i++) {
		Serial.print(arrayData[i]);
		if (i != count - 1) {
			Serial.print(F(","));
		}
	}
	Serial.print(F("]"));
}

void SetJsonValue(byte arrayData[], const byte count, const String key, const JsonObject& root) {
	if (!root.containsKey(key))
		return;
	for (byte i = 0; i < count; i++) {
		arrayData[i] = root[key][i].as<byte>();
	}
}

/**------------------------------------ SAVE TO EEPROM ---------------------------------------*/
void SaveChanalState() {
	for (byte i = 0; i < MAX_CHANALS; i++) {
		EEPROM.write(addrChanals - i, stateChanals[i]);
	}

	GetChanalState();
}

void LoadChanelState() {
	for (byte i = 0; i < MAX_CHANALS; i++) {
		stateChanals[i] = EEPROM.read(addrChanals - i);
	}
	CheckStateAllChanal();
}

void LoadTimersReadFromERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		DailyTimerHourStart[i] = EEPROM.read(DailyTimerHourStartAddr - i);
		DailyTimerHourEnd[i] = EEPROM.read(DailyTimerHourEndAddr - i);
		DailyTimerMinStart[i] = EEPROM.read(DailyTimerMinStartAddr - i);
		DailyTimerMinEnd[i] = EEPROM.read(DailyTimerMinEndAddr - i);
		DailyTimerState[i] = EEPROM.read(DailyTimerStateAddr - i);
		DailyTimerChanal[i] = EEPROM.read(DailyTimerChanalAddr - i);
	}
}

void SaveTimerToERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		EEPROM.write(DailyTimerHourStartAddr - i, DailyTimerHourStart[i]);
		EEPROM.write(DailyTimerHourEndAddr - i, DailyTimerHourEnd[i]);
		EEPROM.write(DailyTimerMinStartAddr - i, DailyTimerMinStart[i]);
		EEPROM.write(DailyTimerMinEndAddr - i, DailyTimerMinEnd[i]);
		EEPROM.write(DailyTimerStateAddr - i, DailyTimerState[i]);
		EEPROM.write(DailyTimerChanalAddr - i, DailyTimerChanal[i]);
	}
}
void LoadSecondsTimersReadFromERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		SecondTimerHourStart[i] = EEPROM.read(SecondTimerHourStartAddr - i);
		SecondTimerMinStart[i] = EEPROM.read(SecondTimerMinStartAddr - i);
		if (EEPROM.read(SecondTimerDurationAddr - i) > 0)
			SecondTimerDuration[i] = EEPROM.read(SecondTimerDurationAddr - i);
		SecondTimerState[i] = EEPROM.read(SecondTimerStateAddr - i);
		SecondTimerCanal[i] = EEPROM.read(SecondTimerCanalAddr - i);
	}
}
void SaveSecondsTimerToERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		EEPROM.write(SecondTimerHourStartAddr - i, SecondTimerHourStart[i]);
		EEPROM.write(SecondTimerMinStartAddr - i, SecondTimerMinStart[i]);
		EEPROM.write(SecondTimerDurationAddr - i, SecondTimerDuration[i]);
		EEPROM.write(SecondTimerStateAddr - i, SecondTimerState[i]);
		EEPROM.write(SecondTimerCanalAddr - i, SecondTimerCanal[i]);
	}
}
void LoadHoursTimersReadFromERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		HoursTimerMinStart[i] = EEPROM.read(DailyHoursTimerMinStartAddr - i);
		HoursTimerMinStop[i] = EEPROM.read(DailyHoursTimerMinDurationAddr - i);
		HoursTimerState[i] = EEPROM.read(DailyHoursTimerStateAddr - i);
		HoursTimerCanal[i] = EEPROM.read(DailyHoursTimerCanalAddr - i);
	}
}
void SaveHoursTimerToERROM() {
	for (byte i = 0; i < MAX_TIMERS; i++) {
		EEPROM.write(DailyHoursTimerMinStartAddr - i, HoursTimerMinStart[i]);
		EEPROM.write(DailyHoursTimerMinDurationAddr - i, HoursTimerMinStop[i]);
		EEPROM.write(DailyHoursTimerStateAddr - i, HoursTimerState[i]);
		EEPROM.write(DailyHoursTimerCanalAddr - i, HoursTimerCanal[i]);
	}
}

void LoadTempTimerFromERROM() {
	for (byte j = 0; j < MAX_TEMP_SENSOR; j++) {
		for (byte i = 0; i < 8; i++) {
			addrThermometer[j][i] = EEPROM.read(addrTempSensor - j * 8 + i);
		}
		ds.setResolution(addrThermometer[j], TEMPERATURE_PRECISION);
	}
	for (byte i = 0; i < MAX_TEMP_SENSOR; i++) {
		TempTimerState[i] = EEPROM.read(TempTimerStateAddr - i);
		TempTimerMinStart[i] = EEPROM.read(TempTimerMinStartAddr - i);
		TempTimerMaxEnd[i] = EEPROM.read(TempTimerMaxEndAddr - i);
		TempTimerChanal[i] = EEPROM.read(TempTimerChanalAddr - i);

	}

}

byte ConvertTempWordToByte(const word temp) {
	return (temp - MIN_TEMP) / STEP;
}

word ConvertTempByteToWord(const byte temp) {
	return temp * STEP + MIN_TEMP;
}

void SaveTempTimerToERROM() {
	for (byte i = 0; i < MAX_TEMP_SENSOR; i++) {
		EEPROM.write(TempTimerStateAddr - i, TempTimerState[i]);
		EEPROM.write(TempTimerMinStartAddr - i, TempTimerMinStart[i]);
		EEPROM.write(TempTimerMaxEndAddr - i, TempTimerMaxEnd[i]);
		EEPROM.write(TempTimerChanalAddr - i, TempTimerChanal[i]);
	}
}

void SaveLcdSetings() {
	EEPROM.write(LCD_LED_ON_ADDR, indexDelayLCDLedOn);
	EEPROM.write(LCD_BACK_ADDR, indexDelayLCDBackInmainScreen);
	EEPROM.write(LCD_BUTTON_ADDR, indexDelayLCDButton);
	EEPROM.write(LCD_SOUND_ADDR, isTone);

}
void LoadLcdSetings() {
	indexDelayLCDLedOn = EEPROM.read(LCD_LED_ON_ADDR);
	indexDelayLCDBackInmainScreen = EEPROM.read(LCD_BACK_ADDR);
	indexDelayLCDButton = EEPROM.read(LCD_BUTTON_ADDR);
	isTone = EEPROM.read(LCD_SOUND_ADDR);
	if (LcdChangeValue(false)) {
		SaveLcdSetings();
	}
}

void LoadWiFiSettings() {
	NTP_UPDATE = (bool) EEPROM.read(NTP_UPDATE_ADDR);
	AUTO_CONNECT = (bool) EEPROM.read(AUTO_CONNECT_ADDR);
}

void SaveWifiSettings() {
	EEPROM.write(NTP_UPDATE_ADDR, (byte) NTP_UPDATE);
	EEPROM.write(AUTO_CONNECT_ADDR, (byte) AUTO_CONNECT);
}
/** ------------------------------------------------------------------------------- */
void OnFirstLunch() {
	if (EEPROM.read(ADDR_FIRST_LAUNCH) != 0) {
		for (unsigned int i = 0; i < EEPROM.length(); i++) {
			EEPROM.write(i, 0);
		}
		SaveTempTimerToERROM();
	}
}
